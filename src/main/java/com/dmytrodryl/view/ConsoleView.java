package com.dmytrodryl.view;

import com.dmytrodryl.Controller;
import com.dmytrodryl.model.Gym;
import com.dmytrodryl.model.PowerTraining;
import com.dmytrodryl.model.ReliefTraining;
import com.dmytrodryl.model.WeightLossTraining;
import java.util.HashMap;
import java.util.Scanner;

public class ConsoleView {

  private static Scanner scanner = new Scanner(System.in);

  public int getUserTraining() {
    System.out.println("1. Weight loss exetcise.");
    System.out.println("2. Relief training.");
    System.out.println("3. Power training.");
    int i = scanner.nextInt();
    return i;
  }

  public void showMenu(Gym gym) {
    System.out.println("1. Choose training");
    System.out.println("2. Exit");
    int choose = scanner.nextInt();
    switch (choose) {
      case 1:
        int i = getUserTraining();
        showTraining(i, gym);
        break;

      case 2:
        System.exit(0);
        break;
      default:
        System.out.println("bad choose, try again");
        Controller.execute();
        break;
    }
  }

  public void showTraining(int i, Gym gym) {
    HashMap<String, String> map;
    switch (i) {
      case 1:
        map = gym.getApparatus(new WeightLossTraining());
        System.out.println(map);
        Controller.execute();

        break;
      case 2:
        map = gym.getApparatus(new ReliefTraining());
        System.out.println(map);
        Controller.execute();
        break;
      case 3:
        map = gym.getApparatus(new PowerTraining());
        System.out.println(map);
        Controller.execute();
        break;
      default:
        System.out.println("bad choose, try again");
        Controller.execute();
    }
  }
}
