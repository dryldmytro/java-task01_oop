package com.dmytrodryl.model;

import java.util.ArrayList;
import java.util.HashMap;

public class Gym implements ModelGym {

  static ArrayList<TrainingApparatus> apparatuses = new ArrayList<>();
  RunningTrack runningTrack = new RunningTrack();
  HorizontalBar horizontalBar = new HorizontalBar();
  Barbell barbell = new Barbell();

  public HashMap<String, String> getApparatus(Exercise exercise) {
    HashMap<String, String> map = new HashMap<>();
    apparatuses.forEach(trainingApparatus -> {
          int levelLoad = exercise.getLevelLoad(trainingApparatus.getStandartLevelLoad());
          int timeForExercise = exercise.getTime(trainingApparatus.getTime());
          String key = trainingApparatus.getName();
          String value =
              " time for exercise: " + timeForExercise + "min. With level load: " + levelLoad;
          map.put(key, value);
        }
    );
    return map;
  }
}
