package com.dmytrodryl.model;

public class WeightLossTraining extends Exercise {

  @Override
  public int getTime(int time) {
    time = time * 2;
    return time;
  }

  @Override
  public int getLevelLoad(int levelLoad) {
    levelLoad = levelLoad / 2;
    return levelLoad;
  }
}
