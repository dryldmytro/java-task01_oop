package com.dmytrodryl.model;

public abstract class TrainingApparatus {

  String name;
  int standartLevelLoad;
  int time;

  public int getTime() {
    return time;
  }

  public void setTime(int time) {
    this.time = time;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getStandartLevelLoad() {
    return standartLevelLoad;
  }

  public void setStandartLevelLoad(int standartLevelLoad) {
    this.standartLevelLoad = standartLevelLoad;
  }
}
