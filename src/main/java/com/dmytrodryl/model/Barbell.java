package com.dmytrodryl.model;

public class Barbell extends TrainingApparatus {

  public Barbell() {
    setName("Barbell");
    setStandartLevelLoad(20);
    setTime(10);
    Gym.apparatuses.add(this);
  }
}
