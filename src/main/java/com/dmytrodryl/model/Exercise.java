package com.dmytrodryl.model;

public abstract class Exercise {
  public int getTime(int time){
    return time;
  }
  public int getLevelLoad(int levelLoad){
    return levelLoad;
  }
}
