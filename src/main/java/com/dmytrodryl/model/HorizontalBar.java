package com.dmytrodryl.model;

public class HorizontalBar extends TrainingApparatus {

  public HorizontalBar() {
    setStandartLevelLoad(10);
    setName("Horizontal bar");
    setTime(20);
    Gym.apparatuses.add(this);
  }
}
