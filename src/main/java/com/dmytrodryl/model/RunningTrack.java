package com.dmytrodryl.model;

public class RunningTrack extends TrainingApparatus {

  public RunningTrack() {
    setName("Running Track");
    setStandartLevelLoad(10);
    setTime(30);
    Gym.apparatuses.add(this);
  }
}
