package com.dmytrodryl.model;

public class PowerTraining extends Exercise {

  @Override
  public int getTime(int time) {
    time = time/3;
    return time;
  }

  @Override
  public int getLevelLoad(int levelLoad) {
    levelLoad = levelLoad*2;
    return levelLoad;
  }
}
