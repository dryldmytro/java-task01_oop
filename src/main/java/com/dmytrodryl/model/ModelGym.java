package com.dmytrodryl.model;

import java.util.HashMap;

public interface ModelGym {

  HashMap<String, String> getApparatus(Exercise exercise);

}
