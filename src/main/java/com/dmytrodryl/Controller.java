package com.dmytrodryl;

import com.dmytrodryl.model.Gym;
import com.dmytrodryl.view.ConsoleView;

public class Controller {

  static Gym gym = new Gym();
  static ConsoleView consoleView = new ConsoleView();

  public static void execute() {
    consoleView.showMenu(gym);
  }

}
